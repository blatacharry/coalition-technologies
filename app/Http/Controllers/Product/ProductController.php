<?php

/**
 * @Author: DevKobby
 * @Date:   2018-10-30 07:37:37
 * @Last Modified by:   DevKobby
 * @Last Modified time: 2018-11-03 08:28:58
 */
namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Product\ProductRequest;

class ProductController extends Controller
{
    public function post(ProductRequest $request)
    {
    	try {
            // my data storage location is project_root/storage/app/data.json file.
            $storageInfo = Storage::disk('local')->exists('data.json') ? json_decode(Storage::disk('local')->get('data.json')) : [];
        
            $inputreq = $request->only(['name', 'stock', 'price']);
           
            $inputreq['datetime_submitted'] = date('Y-m-d H:i:s');
 
            array_push($storageInfo,$inputreq);
    
            Storage::disk('local')->put('data.json', json_encode($storageInfo));
 	
 			return response()->json(['success' => 'Item was successfully posted']);
 
        } catch(Exception $e) {
 
            return response()->json(['message' => $e->getMessage() , 'error' => true]);
 
        }
    }
}

