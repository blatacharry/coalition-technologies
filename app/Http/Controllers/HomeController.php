<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /* $exists = Storage::disk('local')->exists('data.json');

        return $exists;*/

        $data = Storage::get('data.json');
        $data = json_decode($data , true);
        
        
        return view('home.index' , compact('data'));
    }
}
