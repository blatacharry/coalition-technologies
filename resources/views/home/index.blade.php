@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Post a Product') }}</div>

                <div class="card-body">

                    <form id="#ProductForm">
                        <div class="alert alert-success" style="display:none"></div>
                        <div class="alert alert-error" style="display:none"></div>

                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __('Product Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
							<label for="example-number-input" class="col-sm-4 col-form-label text-md-right">Quantity in Stock</label>
							
							<div class="col-md-6">
								<input class="form-control{{ $errors->has('stock') ? ' is-invalid' : '' }}" name="stock" value="{{ old('stock') }}" type="number" value="1" id="stock" required>
							</div>

							@if ($errors->has('stock'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('stock') }}</strong>
                                </span>
                            @endif
						</div>

                        <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>

                            <div class="col-md-6">
                                <input id="price" type="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" required>

                                @if ($errors->has('price'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       	<div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button class="btn btn-primary" id="productSubmit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
            	<table class="table table-striped">
					<thead>
						<tr>
						  <th scope="col">#</th>
						  <th scope="col">Product Name</th>
						  <th scope="col">Price</th>
						  <th scope="col">Quantity</th>
						  <th scope="col">Total Value Number</th>
						  <th scope="col">Date Submitted</th>
						</tr>
					</thead>
			  		<tbody>
			  			@php
			  				$row = 1;
			  			@endphp
						@foreach($data as $d)
							
	                		<tr>
						      <th scope="row">{{ $row }}</th>
						      <td> {!! $d['name'] !!} <td>
						      <td>{!! $d['stock'] !!}</td>
						      <td>{!! $d['price'] !!}</td>
						      <td>{!! $d['stock'] * $d['price']!!}</td>
						      <td>{!! $d['datetime_submitted'] !!}</td>
						    </tr>

						    @php
				  				$row++;
				  			@endphp
	                	@endforeach
			  		</tbody>
				</table>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
	<script src="http://code.jquery.com/jquery-3.3.1.min.js"
               integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
               crossorigin="anonymous">
	</script>
	<script>
		jQuery(document).ready(function(){
		    jQuery('#productSubmit').click(function(e){
				e.preventDefault();
				console.log('me');
				
				jQuery.ajax({
					url: "{{ url('/product') }}",
					type: 'POST',
					
					data: {
						name: jQuery('#name').val(),
						stock: jQuery('#stock').val(),
						price: jQuery('#price').val(),
						'_token': "{{ csrf_token() }}",
						'_method':'POST'
		        	},
					success: function(result){
						jQuery('.alert-success').show();
						jQuery('.alert-success').html(result.success);

						setTimeout(function(){// wait for 5 secs(2)
						   	location.reload(); // then reload the page.(3)
						}, 5000);
					},
					error: function(result){
						jQuery('.alert-error').show();
						jQuery('.alert-error').html(result.message);
					}
				});
	       	});
		});
	</script>
@endsection
